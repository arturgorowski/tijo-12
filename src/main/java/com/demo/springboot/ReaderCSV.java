package com.demo.springboot;

import com.demo.springboot.domain.dto.MovieDto;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

public class ReaderCSV {

    List<MovieDto> movies = new ArrayList<>();

    public List<MovieDto> myRead() {
        try (CSVReader csvReader = new CSVReader(new FileReader("movies.csv"), ';')) {
            String[] values = null;
            csvReader.readNext();
            while ((values = csvReader.readNext()) != null) {
                movies.add(new MovieDto(Integer.parseInt(values[0]), values[1], Integer.parseInt(values[2]), values[3]));
                values = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return movies;
    }
}
