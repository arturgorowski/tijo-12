package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {
    private int movieId;
    private String title;
    private int year;
    private String image;

    public MovieDto(int movieId, String title, int year, String image){
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public int getMovieId(){
        return movieId;
    }
    public String getTitle(){
        return title;
    }
    public int getYear(){
        return year;
    }
    public String getImage(){
        return image;
    }

    // TODO: Pola klasy powinny miec identyczne nazwy jak klucze w JSONie
    // TODO: Bardzo prosze dokonczyc implementacje klasy
}
