package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.ReaderCSV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");

        List<MovieDto> movies;
        ReaderCSV readerCSV = new ReaderCSV();
        movies = readerCSV.myRead();

        // TODO: Usluga powinna realizowac nastepujace zadania:
        // TODO: Prosze odczytac zawartosc pliku movies.csv
        // TODO: Prosze zapisac wszystkie dane z pliku movies.csv do listy znajdujacej sie w klasie MovieListDto
        // TODO: Po skonczonej implementacji prosze przetestowac w przegladarce usluge pod adresem http://127.0.0.1:8080/movies
        // TODO: Nastepnie prosze otworzyc plik movies.html

        return new MovieListDto(movies);
    }
}